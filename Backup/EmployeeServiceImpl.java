package com.employee.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.employee.entities.T_EmployeeInfo;
import com.employee.repository.EmployeeRepo;

@Service
public class EmployeeServiceImpl implements IEmployeeService {
	@Autowired
	EmployeeRepo empRepo; //dao

	@Override
	@Transactional
	public List<T_EmployeeInfo> allEmployees() {	
		return this.empRepo.findAll();
	}

	@Override
	@Transactional
	public EmployeeRepo findById(int id) {
		return (EmployeeRepo) this.empRepo.findById(id).get();
	}

	@Override
	public void deleteEmployee(int id) {
		T_EmployeeInfo emp= empRepo.findById(id).get();
		System.out.println("The employee deleted is: \n" + emp);
		this.empRepo.deleteById(id);
		System.out.println("Employee Deleted Successfully");
		
	}

	@Override
	@Transactional
	public void updateEmployeeDetails(T_EmployeeInfo employeeDetailsUpdate) {
		this.empRepo.save(employeeDetailsUpdate);
		System.out.println("ecord Updated Successfully");
		
	}
	


	
	
}
