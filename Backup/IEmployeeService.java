package com.employee.services;

import java.util.List;

import com.employee.entities.T_EmployeeInfo;

import com.employee.repository.EmployeeRepo;

public interface IEmployeeService {

	 List<T_EmployeeInfo> allEmployees();

	 EmployeeRepo findById(int id);

	void deleteEmployee(int id);

	void updateEmployeeDetails(T_EmployeeInfo employeeDetailsUpdate);

	


}
