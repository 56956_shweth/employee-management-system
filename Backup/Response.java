package com.employee.dto;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Response {

	public static ResponseEntity<?> success(Object dataPresent){
		Map<String, Object> map = new HashMap<>();
		map.put("status", "SUCCESS");
			if(dataPresent != null)
				map.put("data",dataPresent);
		return ResponseEntity.ok(map);
	}
	
	public static ResponseEntity<?> error(Object errorOccured){
		Map<String,Object> map = new HashMap<>();
		map.put("status","ERROR");
			if(errorOccured != null)
				map.put("error", errorOccured);
			return ResponseEntity.ok(map);
				
	}
	
	public static ResponseEntity<?> status(HttpStatus status) {
		return ResponseEntity.status(status).build();
		
	}
	
	
}
