package com.employee.Response;

import lombok.Data;

@Data
public class ErrorResponse {
	private String message;
	private Object data;
	private boolean status;
	
	public ErrorResponse(String message, Object data, boolean status) {
		super();
		this.message = message;
		this.data = data;
		this.status = status;
	}
	
	
}
