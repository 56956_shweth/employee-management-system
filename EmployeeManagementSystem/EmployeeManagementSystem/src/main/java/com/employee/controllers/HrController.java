package com.employee.controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.Response.ErrorResponse;
import com.employee.dto.Response;
import com.employee.entities.T_EmployeeInfo;
//import com.employee.repository.Employee;
import com.employee.repository.EmployeeRepo;
import com.employee.services.IEmployeeService;
import com.employee.services.IHrService;


@CrossOrigin
@RestController //(@Controller + @ResponseBody)
@RequestMapping("/hit/hr") //hr request
public class HrController {

	@Autowired
	private IHrService hrservice;
	
	@Autowired
	private IEmployeeService empService;
	

	/////////////////////////////////////////////////////////////
	@GetMapping
	public ResponseEntity<?> findAllEmployees(){
		List<T_EmployeeInfo> result = empService.allEmployees();
		return Response.success(result);
	}
	
	@GetMapping("/findById/{id}")
	public ResponseEntity<?> findEmployeeProfile(@PathVariable int id){
		EmployeeRepo result = empService.findById(id);
		return Response.success(result);
	}
	
	@DeleteMapping("/deleteEmp/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id){
		empService.deleteEmployee(id);
		return Response.status(HttpStatus.OK);
	}
	
	@PutMapping("/updateEmp/{id}")
	public ResponseEntity<?> updateEmployeeDetails(@RequestBody T_EmployeeInfo employeeDetailsUpdate,@PathVariable("id") int id)
	{
		System.out.println(employeeDetailsUpdate);
		
		empService.updateEmployeeDetails(employeeDetailsUpdate);
		return Response.success(employeeDetailsUpdate);
	}
	
	//////////////////////////////////////////////////////////////
	
	
	
	@GetMapping("/viewEmployees")
	public ResponseEntity<?> viewEmployees() throws IOException {


		
		List<T_EmployeeInfo> viewUser = this.hrservice.viewEmployees();
		if(!viewUser.isEmpty()) {
			return ResponseEntity.ok(new ErrorResponse("fetch successfully",viewUser,true));
		}else {
			return ResponseEntity.ok(new ErrorResponse("failed to fetch",null,false));
		}
	}
	
	
	@GetMapping("/viewEmployee/{id}")
	public  ResponseEntity<?> viewEmployeeById(String id){
		int e_Id= Integer.parseInt(id);
		Optional<T_EmployeeInfo> emp= this.hrservice.viewEmployeeBy(e_Id);
		if(!emp.equals(null)){
			return ResponseEntity.ok(new ErrorResponse("fetch successfully",emp,true));
		}else {
			return ResponseEntity.ok(new ErrorResponse("failed to fetch",null,false));
		}
	}
	
}
