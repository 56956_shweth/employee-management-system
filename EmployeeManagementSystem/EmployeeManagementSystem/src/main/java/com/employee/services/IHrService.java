package com.employee.services;

import java.util.List;
import java.util.Optional;

import com.employee.entities.Hr;
import com.employee.entities.HrEntity;
import com.employee.entities.T_EmployeeInfo;

public interface IHrService {

	List<T_EmployeeInfo> viewEmployees();

	Optional<T_EmployeeInfo> viewEmployeeBy(int e_Id);

	int validateEmployee(String username, String password);



	HrEntity validate(String email, String password);
	
	

}
