package com.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.entities.T_EmployeeInfo;

public interface Hr extends JpaRepository<T_EmployeeInfo, Integer> 
{
	T_EmployeeInfo findByroleCode(T_EmployeeInfo emp_id);
}
                                          