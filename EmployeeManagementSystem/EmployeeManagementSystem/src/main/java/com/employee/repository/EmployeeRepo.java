package com.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.entities.T_EmployeeInfo;

public interface EmployeeRepo extends JpaRepository<T_EmployeeInfo, Integer>
{
	
}
