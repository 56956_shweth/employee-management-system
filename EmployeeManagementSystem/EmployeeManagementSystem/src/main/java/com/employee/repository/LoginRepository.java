package com.employee.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.entities.T_Employee_Credentials;

public interface LoginRepository extends JpaRepository<T_Employee_Credentials, Integer> {

	List<T_Employee_Credentials> findByuserName(String Username);
}
