package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class M_IdentityProof {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="Identity_Proof_ID")
	private int identityProofID;
	@Column(name="Identity_Document")
	private String identityProofDoc;
}
