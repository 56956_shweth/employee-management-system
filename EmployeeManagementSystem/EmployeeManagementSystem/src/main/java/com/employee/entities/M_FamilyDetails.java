package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class M_FamilyDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="Relationship_Code")
	
	private int relationshipCode;
	
	@Column(name="Relationship")
	private String relationshipType;
}
