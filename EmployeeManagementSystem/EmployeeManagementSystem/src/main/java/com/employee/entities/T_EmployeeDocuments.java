package com.employee.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
	@Entity
	@Data
public class T_EmployeeDocuments {
	@ManyToOne
	private T_EmployeeInfo empId;
	private String resume;
	private long aadhar;
	private String panCard;
}
