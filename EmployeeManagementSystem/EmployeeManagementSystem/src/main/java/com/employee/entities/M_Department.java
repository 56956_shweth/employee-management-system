package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class M_Department {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Department_ID")
	private int departmentID;
	@Column(name="Department_Name")
	private String depatrmentName;
}
