package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class M_Role {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="Role_code")
	private int roleCode;
	@Column(name="Role_Name")
	private String roleName;
	
}
