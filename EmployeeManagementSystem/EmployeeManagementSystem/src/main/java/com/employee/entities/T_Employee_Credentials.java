package com.employee.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity

public class T_Employee_Credentials {
	
	@Column(name="fk_emp_id")
	private T_EmployeeInfo emp_id;
	
	@Column(name="userName")
	private String userName;
	
	@Column(name="password")
	private String password;
	
	@Column(name="passwordUpdateFrequency")
    private int passwordUpdateFrequency;
	
	@Column(name="recordCreated")
    private Date recordCreated;
	
	@Column(name="recordUpdated")
    private Date recordUpdated;
    
}
