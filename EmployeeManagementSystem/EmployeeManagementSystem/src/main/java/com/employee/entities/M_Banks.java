package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class M_Banks {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="Bank_ID")
	private int bankId;
	@Column(name="Bank_Name")
	private String bankName;
	@Column(name="Bank_Address")
	private String bankAddress;
}
