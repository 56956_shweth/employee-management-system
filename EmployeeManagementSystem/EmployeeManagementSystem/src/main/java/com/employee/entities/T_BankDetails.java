package com.employee.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class T_BankDetails {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private String accNo;
	private String bankName;
	private String IFSC;

}
