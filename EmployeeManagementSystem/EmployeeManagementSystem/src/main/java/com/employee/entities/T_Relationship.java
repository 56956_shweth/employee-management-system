package com.employee.entities;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;
@Data
@Entity
public class T_Relationship {
	@Column(name="Emp_Id")
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name = "t_Employee_Info", referencedColumnName = "Emp_id")
	  private T_EmployeeInfo empId;
	 
	  @Column(name="Relationship_Code")
	  
	  @JoinColumn(name = "M_FamilyDetails", referencedColumnName = "RelationshipCode")
	  @ManyToOne(cascade=CascadeType.ALL)
	   private M_FamilyDetails relationshipCode;
	 
	  @Column(name="Full_Name")
	  private String fullName;
	 
	  @Column(name="Phone_No")
	  private long phoneNo ;
	  
	  @Column(name="Address")
	  private String address;
	  
	}

