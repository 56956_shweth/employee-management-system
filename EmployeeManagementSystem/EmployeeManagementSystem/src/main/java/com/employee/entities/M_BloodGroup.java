package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class M_BloodGroup {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="BloodGroup_ID")
	private int bloodGroupId;
	@Column(name="Blood_Group_Type")
	private String bloodGroupType;
}
