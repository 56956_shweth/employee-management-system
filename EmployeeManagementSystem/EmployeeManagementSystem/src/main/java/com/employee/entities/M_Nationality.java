package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class M_Nationality {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="Nationality_Id")
	private int nationalityId;
	
	@Column(name="Nationality_Name")
	private String nationalityName;
}
