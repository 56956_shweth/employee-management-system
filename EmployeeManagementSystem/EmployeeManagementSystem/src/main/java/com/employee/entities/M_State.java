package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class M_State {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="StateId")

	private int stateId;
	
	@Column(name="StateName")
	private String stateName;
}
