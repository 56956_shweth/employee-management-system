package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class M_Experience {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="Experience_ID")
	private int experienceStatusID;
	@Column(name="Type_Of_Experience")
	private String typeOfExperience;
}