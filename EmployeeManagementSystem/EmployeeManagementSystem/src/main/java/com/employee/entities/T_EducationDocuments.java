package com.employee.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity
public class T_EducationDocuments {
  @Column(name="Emp_Id")
  private T_EmployeeInfo empId;
 
  @Column(name="Education_Id")
  private M_Education eduDocId;
 
  @Column(name="Education_Documents")
  private String eduDocuments;
 
  @Column(name="Records_Created")
  private Data recordsCreated;
  
  @Column(name="Records_Deleted")
  private Date recordsDeleted;
  
}
