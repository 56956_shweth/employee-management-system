package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class M_Gender {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name="Gender_ID")
	private int genderId;
	@Column(name="Gender_Name")
	private String genderName;
}
