package com.employee.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import lombok.Data;

@Data
@Entity

public class T_Address {
	
	@Column(name="fk_emp_id")
	private T_EmployeeInfo emp_id;
	
	@Column(name="addressLine1")
	private String addressLine1;
	
	@Column(name="addressLine2")
	private String addressLine2;
	
	@Column(name="City")
    private String City;
	
	@Column(name="pinCode")
    private int pinCode;
	
	@Column(name="stateId")
    private int stateId;
	
	@Column(name="recordCreated")
    private Date recordCreated;
	
	@Column(name="recordUpdated")
    private Date recordUpdated;

}
