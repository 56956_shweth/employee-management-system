

//Optional

package com.employee.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data

public class M_LeaveType {
	
	@Id
	private int id;
	private String leaveType;
}
