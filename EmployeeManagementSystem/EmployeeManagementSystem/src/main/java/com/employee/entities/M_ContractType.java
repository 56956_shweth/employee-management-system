package com.employee.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class M_ContractType {


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Contract_Type_ID")
	private int contractTypeId;
	@Column(name="Contract_Type_Name")
	private String contractTypeName;
	
	
}
