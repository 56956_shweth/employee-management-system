package com.employee.entities;

import java.sql.Blob;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Data

public class T_EmployeeInfo {

	@Id
	private int EmpId;
	private String fName;
	private String mName;
	private String lName;
	private String gender;
	
	private M_Designation dId;
	
	private Date bDate;
	private String email;
	private String contact;
	private String address;
	private int typeId;
	private Date joiningDate;
	private Date contractEndDate;
	private String bloodGroup;
	private double salary;
	
	private M_Role roleCode;
	
	private Blob photo;
	
	private M_ContractType cTypeId;
	
	
	
}
